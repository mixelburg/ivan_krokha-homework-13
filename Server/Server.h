#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "MessageSender.h"

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	std::string login(SOCKET clientSocket);
	void updateClient(SOCKET clientSocket, std::string& thisUname);

	SOCKET _serverSocket;
	MessageSender _msgSender;
};

