#pragma comment (lib, "ws2_32.lib")

#include <iostream>
#include <exception>

#include "WSAInitializer.h"
#include "Server.h"

#define SERVER_PORT 8826

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(SERVER_PORT);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}