#include "MessageSender.h"
#include <iostream>
#include <fstream>

bool MessageSender::isConnected(User& user) const
{
	return _connectedUsers.find(user) != _connectedUsers.end();
}

std::string MessageSender::getOutFileName(std::string& from, std::string& to)
{
	std::string names[] = { from, to };
	std::sort(std::begin(names), std::end(names));
	return  names[0] + DELIMITER + names[1] + OUT_FILE_FORMAT;
}

void MessageSender::connectUser(User& user)
{
	if (isConnected(user)) throw UserIsAlreadyConnectedException();
	{
		std::lock_guard<std::mutex> lock(_userSetMutex);
		_connectedUsers.insert(user);
		user.changeStatus();
	}
}

void MessageSender::connectUser(std::string& userName)
{
	User newUser(userName);
	connectUser(newUser);
}

void MessageSender::removeUser(User& user)
{
	if (!isConnected(user)) throw UserIsNotConnectedException();
	_connectedUsers.erase(user);
	user.changeStatus();
}

void MessageSender::removeUser(std::string& username)
{
	User newUser(username);
	removeUser(newUser);
}

void MessageSender::printConnected() const
{
	int num = 0;
	for (const auto& user : _connectedUsers)
	{
		std::cout << num++ << ": " << user.getName() << std::endl;
	}
}

std::string MessageSender::getAllConnected() const
{
	std::string allConnected;
	for (const auto& user : _connectedUsers)
	{
		allConnected += user.getName();
		allConnected += "&";
	}
	return allConnected;
}

void MessageSender::sendMsg(std::string& from, std::string& to, std::string& msg)
{
	std::ofstream outFile;
	const std::string outFileName = getOutFileName(from, to);

	{
		std::lock_guard<std::mutex> writeLock(_sendMsgMutex);
		outFile.open(outFileName, std::ios_base::app);

		if (!outFile.is_open()) throw OpenFileException(outFileName);
		{
			outFile << DELIMITER << "MAGSH_MESSAGE" << DELIMITER;
			outFile << DELIMITER << "Author" << DELIMITER << from;
			outFile << DELIMITER << "DATA" << DELIMITER << msg;
		}
		outFile.close();

	}
}

std::string MessageSender::getMsg(std::string& from, std::string& to)
{
	std::string data;
	std::ifstream inFile;
	const std::string inFileName = getOutFileName(from, to);

	std::lock_guard<std::mutex> writeLock(_sendMsgMutex);
	inFile.open(inFileName);

	if (inFile.is_open())
	{
		data.assign((std::istreambuf_iterator<char>(inFile)),
			(std::istreambuf_iterator<char>()));
	}
	inFile.close();
	return data;
}

