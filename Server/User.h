#pragma once
#include <string>


class User
{
public:
	explicit User(std::string& name);
	User(std::string& name, bool status);
	void changeStatus(); // changes user status to !status
	std::string getName() const;
	friend bool operator==(const User& ths, const User& other);
	friend bool operator<(const User& ths, const User& other);
private:
	std::string _name;
	bool _connected;
};
