#pragma once

#include <mutex>
#include <set>
#include <queue>

#include "User.h"
#include "MyUtil.h"


class OpenFileException final : public std::exception
{
public:
	OpenFileException(const std::string fName, std::string msg = "[!] Error opening file") : std::exception(msg.c_str()), _fileName(fName) {}
	std::string getFile() const { return _fileName; }
private:
	const std::string _fileName;
};

class UserIsNotConnectedException final : public std::exception
{
public:
	const char* what() const throw() override
	{
		return "[!] User is not connected";
	}
};

class UserIsAlreadyConnectedException final : public std::exception
{
public:
	const char* what() const throw() override
	{
		return "[!] User already connected";
	}
};

class MessageSender
{
public:
	/**
	 * @brief adds given User object to connected users list
	 * @param user User object to add
	 * @throws UserIsAlreadyConnectedException if user is already connected
	*/
	void connectUser(User& user);
	void connectUser(std::string& userName); // connects user by name

	/**
	 * @brief removes given User object from list of connected users
	 * @param user User object to remove
	 * @throws UserIsNotConnectedException if given user is not connected
	*/
	void removeUser(User& user);
	void removeUser(std::string& username); // removes user by name

	void printConnected() const; // prints names of all connected users
	std::string getAllConnected() const; // returns string with names of all connected users


	void sendMsg(std::string& from, std::string& to, std::string& msg);
	std::string getMsg(std::string& from, std::string& to);

private:
	std::set<User> _connectedUsers; // list of all connected users
	std::mutex _userSetMutex;
	std::mutex _sendMsgMutex;
	
	bool isConnected(User& user) const; // checks if user is in list of connected

	std::string getOutFileName(std::string& from, std::string& to);

	const int INTERVAL = 5; // repeat interval for readFile function
	const char DELIMITER = '&';
	const std::string OUT_FILE_FORMAT = ".txt";
};



