#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>

#include "Helper.h"

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "[+] Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "[+] Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "[+] Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread newClientThread(&Server::clientHandler, this, client_socket);
	newClientThread.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::string thisUname;
	try
	{
		int inMsgCode;
		do
		{
			inMsgCode = Helper::getMessageTypeCode(clientSocket);

			switch (inMsgCode)
			{
			case MT_CLIENT_LOG_IN:
				thisUname = login(clientSocket);
				break;
			case MT_CLIENT_UPDATE:
				updateClient(clientSocket, thisUname);
				break;
			default:
				std::cout << "Default" << std::endl;
				break;
			}
		} while (inMsgCode != MT_CLIENT_FINISH);
		
		closesocket(clientSocket); 
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		_msgSender.removeUser(thisUname);
		std::cout << "[+] " << thisUname << " disconnected" << std::endl;
	}


}

std::string Server::login(SOCKET clientSocket)
{
	const int unameLen = Helper::getIntPartFromSocket(clientSocket, LengthCode::LC_LoginUname);
	std::string uname = Helper::getStringPartFromSocket(clientSocket, unameLen);

	const std::string blank = "";
	Helper::sendUpdateMessageToClient(clientSocket, blank, blank,
		_msgSender.getAllConnected());

	try
	{
		_msgSender.connectUser(uname);
	}
	catch (const UserIsAlreadyConnectedException& e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (...)
	{
	}

	std::cout << "[+] New Login: " << uname << std::endl;
	std::cout << "[+] All users: " << std::endl;
	_msgSender.printConnected();
	
	return uname;
}

void Server::updateClient(SOCKET clientSocket, std::string& thisUname)
{
	const int lenUname = Helper::getIntPartFromSocket(clientSocket, LC_SecondUname);
	std::string uname = Helper::getStringPartFromSocket(clientSocket, lenUname);

	const int lenMsg = Helper::getIntPartFromSocket(clientSocket, LC_NewMessage);
	std::string msg = Helper::getStringPartFromSocket(clientSocket, lenMsg);

	if (lenMsg != 0)
	{
		_msgSender.sendMsg(thisUname, uname, msg);
		std::cout << "User: " << thisUname << " sent: " << msg << " to: " << uname << std::endl;
	}

	Helper::sendUpdateMessageToClient(clientSocket,
		_msgSender.getMsg(thisUname, uname),
		uname, _msgSender.getAllConnected());
}

